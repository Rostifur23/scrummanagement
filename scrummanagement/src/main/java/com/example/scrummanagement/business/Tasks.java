package com.example.scrummanagement.business;

import java.sql.Date;

public class Tasks {

	private int jira_no;
	private String task_name;
	private String owner;
	private Date uat_date;
	private String status;
	private int tableid;
	private String updateStatus;
	
	
	public Tasks(int jira_no, String task_name, String owner, Date uat_date, String status, int tableid, String updateStatus) {
		super();
		this.jira_no = jira_no;
		this.task_name = task_name;
		this.owner = owner;
		this.uat_date = uat_date;
		this.status = status;
		this.tableid = tableid;
		this.updateStatus = updateStatus;
	}

	public String getUpdateStatus() {
		return updateStatus;
	}

	public void setUpdateStatus(String updateStatus) {
		this.updateStatus = updateStatus;
	}

	public int getJira_no() {
		return jira_no;
	}

	public void setJira_no(int jira_no) {
		this.jira_no = jira_no;
	}

	public String getTask_name() {
		return task_name;
	}

	public void setTask_name(String task_name) {
		this.task_name = task_name;
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public Date getUat_date() {
		return uat_date;
	}

	public void setUat_date(Date uat_date) {
		this.uat_date = uat_date;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public int getTableid() {
		return tableid;
	}

	public void setTableid(int tableid) {
		this.tableid = tableid;
	}
	
	
	
}
