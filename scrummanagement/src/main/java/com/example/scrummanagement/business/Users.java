package com.example.scrummanagement.business;

public class Users {
	private int soeid;
	private String name;
	private String email;
	private String password;
	private String role;
	private Boolean secondscrummaster;
	private Boolean isScrumMaster;
	
	public Users(int soeid, String name, String email, String password, String role, Boolean secondscrummaster,
			Boolean isScrumMaster) {
		super();
		this.soeid = soeid;
		this.name = name;
		this.email = email;
		this.password = password;
		this.role = role;
		this.secondscrummaster = secondscrummaster;
		this.isScrumMaster = isScrumMaster;
	}
	
	
	public int getSoeid() {
		return soeid;
	}

	public void setSoeid(int soeid) {
		this.soeid = soeid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public Boolean getSecondScrummaster() {
		return secondscrummaster;
	}

	public void setSecondScrummaster(Boolean secondscrummaster) {
		this.secondscrummaster = secondscrummaster;
	}

	public Boolean getIsScrumMaster() {
		return isScrumMaster;
	}

	public void setIsScrumMaster(Boolean isScrumMaster) {
		this.isScrumMaster = isScrumMaster;
	}
		
}
