package com.example.scrummanagement.business;

public class ChangePassword {
	private int soeid;
	private String new_password;
	private String old_password;
	private String con_password;
	
	public ChangePassword(int soeid, String new_password, String old_password, String con_password) {
		super();
		this.soeid = soeid;
		this.new_password = new_password;
		this.old_password = old_password;
		this.con_password = con_password;
	}
	public int getSoeid() {
		return soeid;
	}
	public void setSoeid(int soeid) {
		this.soeid = soeid;
	}
	public String getNew_password() {
		return new_password;
	}
	public void setNew_password(String new_password) {
		this.new_password = new_password;
	}
	public String getOld_password() {
		return old_password;
	}
	public void setOld_password(String old_password) {
		this.old_password = old_password;
	}
	public String getCon_password() {
		return con_password;
	}
	public void setCon_password(String con_password) {
		this.con_password = con_password;
	}
	
	
	
}
