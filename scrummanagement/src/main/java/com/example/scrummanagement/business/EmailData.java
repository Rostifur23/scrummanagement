package com.example.scrummanagement.business;

import java.util.List;

public class EmailData {
	private List<String> tasks;

	public List<String> getTasks() {
		return tasks;
	}

	public void setTasks(List<String> tasks) {
		this.tasks = tasks;
	}
	
}
