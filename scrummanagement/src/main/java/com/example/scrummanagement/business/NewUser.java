package com.example.scrummanagement.business;

public class NewUser {

	private int soeid;
	private String name;

	public NewUser(int soeid, String name) {
		super();
		this.soeid = soeid;
		this.name = name;
	}

	public int getSoeid() {
		return soeid;
	}

	public void setSoeid(int soeid) {
		this.soeid = soeid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	
}
