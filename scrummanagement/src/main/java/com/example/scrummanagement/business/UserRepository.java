package com.example.scrummanagement.business;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.example.scrummanagement.dao.Data;
import com.example.scrummanagement.dao.LoginReturnStatus;
import com.example.scrummanagement.dao.ToggleSM;

@Component
public class UserRepository {

	private Data repository;
	
	
	@Autowired
	public UserRepository(Data repository) {
		this.repository = repository;
	}
	
	public Users newMember(Users user) {
		return repository.newMember(user);
	}
	
	public List<Users> allUsers(){
		return repository.allUsers();
	}
	
	public List<Releases> allReleases(){
		return repository.allReleases();
	}
	
	
	public List<Tasks> allTasks(int id){
		return repository.allTasks(id);
	}
	
	public Tasks addTasks(Tasks t) {
		return repository.addTask(t);
	}
	
	public LoginReturnStatus userLogin(String email, String password) {
		return repository.loginStatus(email, password);
	}
	
	//Insert the update status into tasks based on jira number
	//Assumption: The user clicks on one of the rows from the tasks DB (UI)
	public String updateSpace(String update, int jira) {
		return repository.updateSpace(update, jira);
	}
	
	public int deleteMember(int soeid) {
		return repository.deleteMember(soeid);
	}
	
	public String updateTaskStatus(String taskstatus, int jiranumber) {
		return repository.updateTaskStatus(taskstatus, jiranumber);
		
	}
	
	public String changePassword(ChangePassword changepassword) {
		return repository.changePassowrd(changepassword);
	}
	
	public String changeSMRole(ToggleSM toogleScrumMaster) {
		return repository.changeSMRole(toogleScrumMaster);
	}
	
	public Releases newReleases(Releases newReleases) {
		return repository.newRelease(newReleases);
	}
	
	public ReleaseStatus updateReleaseStatus(ReleaseStatus taskStatusUpdate) {
		return repository.updateReleaseStatus(taskStatusUpdate);
	}
	
	public NewUser changeName(NewUser changeName) {
		return repository.changeName(changeName);
	}
}
