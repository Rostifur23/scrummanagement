package com.example.scrummanagement.business;

public class ReleaseStatus {

	private int tableid;
	private String status;
	
	public ReleaseStatus(int tableid, String status) {
		super();
		this.tableid = tableid;
		this.status = status;
	}

	public int getTableid() {
		return tableid;
	}

	public void setTableid(int tableid) {
		this.tableid = tableid;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	
}
