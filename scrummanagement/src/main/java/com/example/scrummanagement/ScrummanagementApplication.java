package com.example.scrummanagement;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ScrummanagementApplication {

	public static void main(String[] args) {
		SpringApplication.run(ScrummanagementApplication.class, args);
	}

}
