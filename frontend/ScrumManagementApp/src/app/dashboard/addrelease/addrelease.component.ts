import { Component, OnInit } from '@angular/core';
import { FormBuilder, NgForm } from '@angular/forms';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-addrelease',
  templateUrl: './addrelease.component.html',
  styleUrls: ['./addrelease.component.scss']
})
export class AddreleaseComponent implements OnInit {

  constructor(private formBuilder: FormBuilder, private httpClient: HttpClient) { }

  RELEASE_URL = "http://localhost:8082/upload";

  ngOnInit() {
  }

  onSubmit(form: NgForm){
        
    console.log(form.value);
    
    this.httpClient.post<any>(this.RELEASE_URL, form.value).subscribe(
      response => {
        console.log(response);
      },
      (err) => console.log(err)
    );

    form.reset();
    
  }

}
