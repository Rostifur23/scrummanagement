export class Member {
    soeid: number;
    name: string;
    email: string;
    password: string;
    role: string;
    secondScrummaster: boolean;
    isScrumMaster: boolean;
  }
  