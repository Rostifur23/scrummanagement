import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  constructor(private router: Router, private httpClient: HttpClient) {}

  settings: boolean;
  addRelease: boolean;
  members: boolean;
  isScrumMaster: boolean;
  SENDMAIL_URL = "http://localhost:8082/dailyUpdateMail";


  ngOnInit() {
    this.members = false;
    this.settings = false;
    this.addRelease = false;
    if(localStorage.getItem("status") != "true"){
      console.log("Redirect");
      this.router.navigate(['/']);
    }

    if((localStorage.getItem("sm") == "true") || (localStorage.getItem("ssm") == "true")){
      this.isScrumMaster = true;
      this.addRelease = true;
    }
  }

  logOut() {

    localStorage.removeItem("status");
    localStorage.removeItem("ssm");
    localStorage.removeItem("sm");

  }

  showSettings(){
    event.preventDefault();
    this.settings = true;
    this.members = false;
    this.addRelease = false;
  }

  showMembers(){
    event.preventDefault();
    this.members = true;
    this.settings = false;
    this.addRelease = false;
  }

  showAddRelease(){
    event.preventDefault();
    this.addRelease = true;
    this.members = false;
    this.settings = false;
  }

  sendEmail(){
    this.httpClient.get<any>(this.SENDMAIL_URL).subscribe(
      res => {
        console.log(res);
      },
      (err) => console.log(err));
  }





}
